# sast_visualizer

> Visualizes GitLab SAST JSON Report

## Build Setup

``` bash
# install dependencies
npm install

# serve with hot reload at localhost:8080
npm run dev

# build for production with minification
npm run build

# build for production and view the bundle analyzer report
npm run build --report
```

For a detailed explanation on how things work, check out the [guide](http://vuejs-templates.github.io/webpack/) and [docs for vue-loader](http://vuejs.github.io/vue-loader).

# License

This software is released under the MIT License, see LICENSE.

This software was created using the following boilerplate.

```
Webpack 5 Vue.js Boilerplate
Copyright (c) 2021 Per Olsen.
Licensed under the MIT License (MIT), see
https://github.com/persteenolsen/webpack-5-vue-boilerplate
```